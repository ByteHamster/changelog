package com.bytehamster.changelog;

import org.junit.Test;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;

import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

public class DevicesTest {

    public DevicesTest() {
        // Needed for tests to pass
    }

    @Test
    public void testDeviceDefinitionsValid() {
        InputStream is = DevicesTest.class.getClassLoader().getResourceAsStream("projects.xml");
        ArrayList<Map<String, Object>> devicesList = Devices.parseDefinitions(is, "");

        assertNotNull(devicesList);

        for (Map<String, Object> device : devicesList) {
            assertNotNull(device.get("name"));
            assertNotNull(device.get("code"));
            assertNotNull(device.get("device_element"));
        }
    }

    @Test
    public void testDeviceDefinitionsNoDuplicates() {
        InputStream is = DevicesTest.class.getClassLoader().getResourceAsStream("projects.xml");
        ArrayList<Map<String, Object>> devicesList = Devices.parseDefinitions(is, "");

        HashSet<String> seenNames = new HashSet<>();
        HashSet<String> seenCodes = new HashSet<>();
        for (Map<String, Object> device : devicesList) {
            String name = device.get("name").toString().toLowerCase();
            String code = device.get("code").toString().toLowerCase();

            if (seenNames.contains(name)) {
                fail("Duplicate device name: " + name);
            } else {
                seenNames.add(name);
            }

            if (seenCodes.contains(code)) {
                fail("Duplicate device code: " + code);
            } else {
                seenCodes.add(code);
            }
        }
    }

    @Test
    public void testDeviceDefinitionsFilterNonExisting() {
        InputStream is = DevicesTest.class.getClassLoader().getResourceAsStream("projects.xml");
        ArrayList<Map<String, Object>> devicesList = Devices.parseDefinitions(is, "this-is-not-a-device");
        assertNotNull(devicesList);
        assertTrue(devicesList.isEmpty());
    }

    @Test
    public void testDeviceDefinitionsFilter() {
        InputStream is = DevicesTest.class.getClassLoader().getResourceAsStream("projects.xml");
        ArrayList<Map<String, Object>> devicesList = Devices.parseDefinitions(is, "i9300");
        assertNotNull(devicesList);

        boolean found = false;
        for (Map<String, Object> device : devicesList) {
            if (device.get("name").equals("Samsung Galaxy S III")) {
                found = true;
            }
        }
        assertTrue(found);
    }
}
