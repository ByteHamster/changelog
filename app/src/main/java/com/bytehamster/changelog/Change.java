package com.bytehamster.changelog;

import android.content.Context;
import android.view.View;
import java.util.HashMap;

class Change {
    public static final int TYPE_ITEM   = 0;
    public static final int TYPE_HEADER = 1;
    //public static final int                      TYPE_MAX_COUNT       = 2;

    public String id       = "";
    public String branch   = "";
    public String number   = ""; // Website link
    public String project  = "";
    public String dateFull = "";
    public String dateDay  = "";
    public String owner    = "";
    public String author   = "";
    public String title    = "";
    public String message  = "";
    public long   date     = 0;
    public long   lastModified  = 0;
    public boolean isNew   = false;


    HashMap<String, Object> getHashMap(Context c) {
        HashMap<String, Object> new_item = new HashMap<String, Object>();
        new_item.put("title", title);

        String secondline = c.getResources().getString(R.string.owner_and_date)
                .replace("%d", dateFull);
        if (owner.equals(author) || author.equals("")) {
            secondline = secondline.replace("%o", owner);
        } else {
            secondline = secondline.replace("%o", owner + " / " + author);
        }

        String messageWithoutTitle = message.substring(message.indexOf('\n')+1);

        new_item.put("secondline", secondline);
        new_item.put("owner", owner);
        new_item.put("author", author);
        new_item.put("dateFull", dateFull);
        new_item.put("project", project);
        new_item.put("number", number);
        new_item.put("type", TYPE_ITEM);
        new_item.put("branch", branch);
        new_item.put("change_id", id);
        new_item.put("is_new", isNew);
        new_item.put("message", message);
        new_item.put("expand", c.getResources().getString(R.string.expanded_message)
                .replace("%project", project)
                .replace("%message", StringTools.filterCommitMessage(message, c))
                .replace("%branch", branch));
        new_item.put("visibility", View.GONE);
        return new_item;
    }

    void calculateDate() {
        dateFull = Main.mDateFormat.format(date);
        dateDay  = Main.mDateDayFormat.format(date);
    }
}
