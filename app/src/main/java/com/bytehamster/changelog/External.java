package com.bytehamster.changelog;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

class External {

    public static void feedbackMail(Context c, String subject, String text) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL, new String[] { "info@bytehamster.com" });
        i.putExtra(Intent.EXTRA_SUBJECT, subject);
        i.putExtra(Intent.EXTRA_TEXT, text);
        try {
            c.startActivity(Intent.createChooser(i, "Send mail..."));
        }
        catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(c, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }

    public static void requestNewDevice(Context c) {
        String newIssueUrl = "https://gitlab.com/ByteHamster/changelog/issues/new?issue[title]=Device+request&issue[description]=";
        String issueText = "Manufacturer: " + Build.MANUFACTURER
                + "\nModel: " + Build.MODEL
                + "\nDevice: " + Build.DEVICE;
        try {
            newIssueUrl += URLEncoder.encode(issueText, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(newIssueUrl));
        try {
            c.startActivity(browserIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(c, "No browser installed", Toast.LENGTH_LONG).show();
        }
    }
}