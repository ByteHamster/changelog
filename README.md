> :warning: **Unmaintained** :warning:  
> This project is currently not maintained. If you are interested in working on this, just create a new issue and introduce yourself :)

# OmniROM Changelog

This changelog shows every merged change on the OmniROM Gerrit instance after your current build time.

<img src='https://gitlab.com/ByteHamster/changelog/raw/master/app/src/main/play/listings/en-US/graphics/icon/icon.png' width='100'/>

### Download 

<a href='https://f-droid.org/packages/com.bytehamster.changelog/'><img src='https://gitlab.com/fdroid/artwork/raw/master/badge/get-it-on.png' width='200'/></a>

### Screenshots

<img src='https://gitlab.com/ByteHamster/changelog/raw/master/app/src/main/play/listings/en-US/graphics/phone-screenshots/00.png' width='200'/>
<img src='https://gitlab.com/ByteHamster/changelog/raw/master/app/src/main/play/listings/en-US/graphics/phone-screenshots/01.png' width='200'/>
<img src='https://gitlab.com/ByteHamster/changelog/raw/master/app/src/main/play/listings/en-US/graphics/phone-screenshots/02.png' width='200'/>

### Contributing

Contributions are always welcome. Just open a merge request!

#### Adding device definitions

    device-definition-converter.py android_device_samsung_i9300
